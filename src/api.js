//for emitting data to the server
//TODO eventually will add single, double, and triple presses!
import { Score, DontPressButton, ShareDiv, EmptyDiv } from './App';
import React from 'react';
import ChatBoard from './ChatBoard';
import { UsernameForm, Username } from './username';
import io from 'socket.io-client';
//this is for testing environment, but would actually need to link to the nodeJS route...
//let socket = io('http://localhost:8080'); //using 'let' instead of 'const'
let socket = io('https://dont-press-that3.herokuapp.com/');
socket.connect();
//socket.connect('https://dont-press-that3.herokuapp.com/', { autoConnect: true });

class SocketComponent extends React.Component {
	//state = { data: {} }
	state = {
		score : 0,
		remaining_time : ""

	}
	constructor(props){
		super(props);
		socket.on('initialize', (name) =>{				
			console.log("Sending Name Over");
			socket.emit('initialize', "LOGGING IN...");
		});
		this.state = {
			isHidden : false,
			score : 0,
			users_count : "Loading All Players",
			all_scores : 0,
			name : "LOGGING IN...",
			player_rank : "Loading...",
		}
	}

	hideForm(){
		this.setState({
			isHidden: !this.state.isHidden
		});
	}

	setUsername(new_name){
		console.log('SETTING NEW USERNAME TO: ', new_name);
		this.setState({
			name : new_name
		});
	}

	componentDidMount(){
		socket.on('points', (n_playerPackage) =>{
			//gets the new points value from server
			console.log("new points value received: ", n_playerPackage);
			var n_points = n_playerPackage.points;
			var n_time = n_playerPackage.time;
			//console.log("n_points is: ", n_points);
			//display these points in the UI

			this.setState ({
				score : n_points === undefined ? 0 : n_points,
				remaining_time : n_time
			});
			// <App state.props.score = my_score />
		});
		socket.on('users_count', (users_count) => {
			console.log("new users_count value received: ", users_count);
			this.setState ({
				users_count : users_count
			});
		});
		socket.on('leaderboard_update', (all_scores) => {
			console.log("received all leaderboard scores array: ", all_scores);
			this.setState ({
				all_scores : all_scores
			});
		});
		socket.on('playerrank', (new_rank) => {
			console.log("received new player rank data: ", new_rank);
			this.setState ({
				player_rank : new_rank
			});
		});
	}

	//add components which display server data in here (require socket behaviour)
	render(){
		return (
			<div>
				<CurrentUsersCount users_count = { this.state.users_count } />
				
				<div id="socket-div">
					{
						this.state.isHidden ?  null : <UsernameForm socket = { socket } setUsername = { this.setUsername.bind(this) } isHidden = { this.hideForm.bind(this) } />
					}
				</div>
				<div>
					<div className='user-area'>
						<div className="top-area">
							<LeaderBoard 
								fresh_scores = { this.state.all_scores } 
								player_rank = { this.state.player_rank }  
								username = { this.state.name }
								score = { this.state.score }
								/>
							<EmptyDiv/>
							<ShareDiv/>
						</div>
						
						<div className='right-column'>
							<Username username= { this.state.name } socket={ socket } />
							<Score score= { this.state.score } />
						</div>
					</div>



				</div>
			</div>
		);
	}
}
///	       			<div>
//	       				<ChatBoard />
//	       			</div>
			        // <div className="button" > 
	          // 			<DontPressButton text="Press" remaining_time = { updateRemainingTime(this.state.remaining_time) } />
	       			// </div>
///
class CurrentUsersCount extends React.Component {
	constructor(props){
		super(props);
		
		this.state = {
			users_count : this.props.users_count
		};
		//console.log("PROPS USERSCOUNT: ", this.props.user_counts);
	}

	componentWillReceiveProps(nextProps){
	    //console.log("COMPONENTWILLRECIEVEPROPS WAS CALLED: ", nextProps);
	    if(this.state.users_count !== nextProps.users_count){
	      this.setState({
	        users_count : nextProps.users_count
	      });
	    }
  	}

	render(){
		return(
			<div>
				<span>Currently Online: </span>
				<span>{ this.state.users_count }</span>
			</div>
		);
	}
}

///
class LeaderBoard extends React.Component {
  constructor(props){
	super(props);
	this.state = {
		all_scores : this.props.fresh_scores,
		player_rank : this.props.player_rank,
		username : this.props.username,
		score : this.props.score,
		rows: []
	}
	//this.addRow = this.addRow.bind(this);
  }
  componentWillReceiveProps(nextProps){
  	console.log("UPDATING PROPS FOR LEADERBOARD: ", nextProps.fresh_scores);
  	
  	console.log("this.state.rows.length: ", this.state.rows.length);
  	this.state.rows = [];
  	if(this.state.all_scores !== nextProps.fresh_scores){
  		//this.setState({ rows : [] });
  		for(var i in nextProps.fresh_scores){
  			this.state.rows.push(nextProps.fresh_scores[i]);
  		}
  		this.setState({
  			all_scores : nextProps.fresh_scores,
  		});
  	}
  	if(this.state.player_rank !== nextProps.player_rank){
  		this.setState({
  			player_rank : nextProps.player_rank
  		});
  	}
  	if(this.state.username !== nextProps.username){
  		this.setState({
  			username : nextProps.username
  		});
  	}
  	if(this.state.score !== nextProps.score){
  		this.setState({
  			score : nextProps.score
  		});
  	}
  	//console.log("this.state.rows.map: ", this.state.rows.map(row => ));
  }
  // addRow(row){
  // 	console.log("ADDING ROW: ", this.state.all_scores);
  // 	var nextState = this.state;

  // 	//nextState.rows.push()
  // }	
  //to highlight on user row, need to know which one is your own row...by comparing push keys and receiving own push key from server
  render(){
    return (
    	<div className='left-column'>
	      <table className="leaderboard">
	        <thead>
	          <tr> 
	            <th>{"Leaders: "}</th>
	          </tr>
	        </thead>
	        <tbody>
	        	{ this.state.rows.map((scorepacket, i) => <tr key={i}>
	        			
	        			<td className="user-row"> { scorepacket.username } </td>
	        			<td className='score-text'> { scorepacket.score } </td>
	        	</tr>)}
	        </tbody>
	      </table>
	      	
	      	{
	      		(this.state.rank > 10) ? 
	      		<table className="playerRow">
		      		<thead>
		      			<tr>
		      				<td>{ this.state.player_rank }</td>
		      				<td>{ this.state.username }</td>
		      				<td>{ this.state.score }</td>
		      			</tr>
		      		</thead>
		      	</table>	
		      		: null 
		    }
      	
      	</div>
      	
      
    );
  }
}
///<td> { scorepacket.rank } </td>
///for packet receiving leaderboardscores from server
function ScorePacket(rank, username, score){
  this.rank = rank;
  this.username = username;
  this.score = score;
}
// function refreshRows(){
//   //query the database
// }

class LeaderBoardRow extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      rank : "UNKNOWN",
      name : "",
      score : 0
    }
  }

  componentDidMount(){
    this.setState({
      rank : this.state.rank, 
      name : this.state.name,
      score : this.state.score
    })
  }

  render(){
      return (
        <tr className="leaderboardrow">
          <td>
            { "Rank: " + this.state.rank }
          </td>
          <td>
          	{" Name: " + this.state.name }
          </td>
          <td>
          	{ " Score: " + this.state.score }
          </td>
        </tr>
      );
    }
}


// look at clock updating example in React library
//use clock which has its hands spin around
function updateRemainingTime(remaining_time){
  switch(remaining_time){
    case 2 : 
    case 3 : {
      return remaining_time--;
    }
    default : {
      return "";
    }
  }
}
//function sendUsername(){
	//socket.on('connect', function(){

	//});
//}

function playerPackage(points, time){
  this.points = points;
  this.time = time;
}

function pressButton(){
	console.log('press button api.js called');
	socket.emit('buttonPressed', Date.now());
}

export {
	pressButton
}

export {
	SocketComponent
}
