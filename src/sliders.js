import React from 'react';


class Slider extends React.Component{
	showLeft(){
		this.refs.left.show();
	}
	showRight(){
		this.refs.right.show();
	}
	render(){
		return (
			<div>
			<button onClick={this.showLeft}>Score Rankings</button>
			<button onClick={this.showRight}>Sharing Options</button>

			<Menu ref="left" alignment="left">
				<MenuItem hash="first-page">First Page</MenuItem>
				<MenuItem hash="second-page">Second Page</MenuItem>
				<MenuItem hash="third-page">Third Page</MenuItem>
			</Menu>

			<Menu ref="right" alignment="right">
				<MenuItem hash="first-page">First Page</MenuItem>
				<MenuItem hash="second-page">Second Page</MenuItem>
				<MenuItem hash="third-page">Third Page</MenuItem>
			</Menu>
		</div>
		);
	}
}
///
//var Menu = React.createClass({
class Menu extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			visible: false
		}
	}

	show(){
		this.setState({
			visible: true
		});
		document.addEventListener("click", this.hide.bind(this));
	}

	hide(){
		this.setState({
			visible: false
		});
		document.removeEventListener("click", this.hide.bind(this));
	}
	//this.props.children is the nner children of the rendered HTML tag, which is the MenuItem objects.
	render(){
		return (
			<div className="menu">
			<div className={(this.state.visible ? "visible " : "") + this.props.alignment}>{this.props.children}</div>
		</div>
		);
	}
}
//what is this 'navigate' method?
class MenuItem extends React.Component{
	navigate(hash){
		window.location.hash = hash;
	}
	//calls for children of the parent, whcih is the simple string text
	render(){
		return (
			<div className="menu-item" onClick={this.navigate.bind(this, this.props.hash)}>{this.props.children}</div>
		);
	}
}

export {Slider}