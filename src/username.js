import React from 'react';
import './username.css';
import axios from 'axios';

//export default
export class UsernameForm extends React.Component {
	constructor(props){
		super(props);
		let isHidden = false;
		this.state 		  = { 
			name: "LOGGING IN..." ,
			isHidden: isHidden
		} ;
		this.handleChange = this.handleChange.bind(this);
		this.socket = this.props.socket;
		this.isHidden = this.props.isHidden;
		this.onSubmit.bind(this);
		this.getWord.bind(this);
	}

	handleChange (event){
		this.setState({
			name: event.currentTarget.value
		});
		console.log("EMITTING USERNAME TO SERVER");
		//this.socket.emit('username', this.state.name);
	}

	getWord = (name) => {
		axios.get('https://setgetgo.com/randomword/get.php')
			.then(res => {
				const random_word = res.data;
				console.log("random word is: ", random_word);
				name += random_word;
				// var new_name = function(name){
				// 	axios.get('http://setgetgo.com/randomword/get.php')
				// 		.then(res => {
				// 			const second_random_word = res.data;
				// 			console.log("second random word is: ", second_random_word);
				// 			return name + " " + second_random_word;
				// 		});
				// }
				//count ++;
				//if(count == 2){

					this.isHidden();
					//var nn = new_name(name);
					//console.log("combined word is: ", nn);
					this.props.setUsername(name);
				//}
			});
	}
	onSubmit = (e) => {
		if(e) e.preventDefault();
		//console.log(this.refs);
		var name = this.refs.usernameItem.value;
		//console.log("Chosen Username is: ", name);
		if(name === ""){
			//set a random name
			//using words API
			//needs mashape authentication...
			//axios.get('https://wordsapiv1.p.mashape.com/words/?random=true')
			//so using setgetgo.com
			//var count = 0;
			//for(var i = 0; i < 2; i++){
				this.getWord(name);//(name);
			// 			axios.get('http://setgetgo.com/randomword/get.php')
			// .then(res => {
			// 	const random_word = res.data;
			// 	console.log("random word is: ", random_word);
			// 	name += random_word + " ";
			// 	//count ++;
			// 	//if(count == 2){
			// 		this.isHidden();
			// 		this.props.setUsername(name);
			// 	//}
			// });
			//}
		}
		else{
			//close the window
			this.isHidden();
			this.props.setUsername(name);
			//console.log("isHidden changing state: ",  );
		}


			
		


		//console.log("onSubmit() called: ", e);
		//console.log("ONSUBMIT CALLED WITH REFS: ", this.refs);
		//console.log("HIDE IS: ", isHidden);
		//isHidden();
	}

//(All ages and sizes)
	render() {
		return (
			<div id="background-div">
				<div id="intro-div">
					<p></p>
					<h1>{"Dont Press The Button"}</h1>
					<span id="intro-desc">{ "Welcome to the app for people who like to press buttons" }</span>
				
					<p><b>P.S.</b> If you don't enter a username you <i>will</i> get a weird awkward name</p>

					<div>
						<form onSubmit = { this.onSubmit }>
							<input type="text" ref = "usernameItem" placeholder="Enter Your Name Here" className="username-input" onChange= { this.handleChange }/>
							<input type="submit" value="Get Started" id="intro-button" />
						</form>
					</div>
					<div>
						
					</div>
				</div>
			</div>
		);
	}
	///can either do ref = "usernameItem", or use ES6 version, ref={(element) => { this.input = element}} and call this.input.value for the ref
}
	// <p>Who like to press buttons </p>
//<p>{"We welcome all ages and sizes"}</p>

///submitUsername(this.state.socket, this.state.name, this.state.hide)
//i get it now, including the brackets meant that the function was being called IMMEDIATELY instead of waiting for a bit and being called.
var count = 0;

export class Username extends React.Component {
	constructor(props){
		super(props);
		//console.log("THIS.PROPS: ", this.props);
		this.state 			= { 
			username: this.props.username,
		};

		//console.log("THIS.STATE.USERNAME: ",this.state.username);
	}
	componentWillReceiveProps(nextProps){
		//console.log("PREVIOSU USERNAME: ", this.state.username);
		//console.log("CURRENT USERNAME: ", nextProps.username);
		var t_now = this.state.username;
		var t_new = nextProps.username;
		if(t_now !== t_new && count === 0){//
			count++;
			//TODO this shoudl be checking to see fi the username is the same or not.
			console.log("SETTING NEW USERNAME STATE");
			this.setState({
				username : nextProps.username
			});
		//}
		//update database details
			console.log("EMITTING USERNAME: ", nextProps.username);
			nextProps.socket.emit('username', nextProps.username);
		}

	}

	render(){
		return (
			<p className="username-text">{this.state.username}</p>
		)
	}
}


///<UsernameButton hide = { this.hide } socket = { this.socket } name={this.state.name} />
					// <p>{"Just let everyone know what you want to be known as"}</p>
///
// class UsernameButton extends React.Component {
// 	constructor(props){
// 		super(props);
// 		let isHidden = null;
// 		this.state = {
// 			hide : this.props.hide,
// 			socket : this.props.socket,
// 			name : this.props.name
// 		}
// 		console.log("refs is: ", this.refs.usernameInput.value);
// 		//console.log("socket is: ", this.state.socket);
// 		//console.log("USERNAME_TEXT' HIDE: ", this.state.hide);
// 	}

// 	componentWillMount(){
// 		//this.state.hide();
// 	}
// 	render(){
// 		return (
// 			<button type="button" id='intro-button' value="Get Started" 
// 			ref = { (hideDiv) => this.props.hide }
// 			onClick={  
// 				this.onSubmit
// 			} >Get Started</button>
// 		);
// 	}
// }