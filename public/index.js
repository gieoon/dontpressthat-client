import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
//import my_score from './App';
import registerServiceWorker from './registerServiceWorker';
//console.log("MY_SCORE: ", my_score);
ReactDOM.render(<App score="0"/>, document.getElementById('root'));
registerServiceWorker();
