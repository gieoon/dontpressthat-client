//CLIENTSIDE CODE

/*global FB*/
//define FB as a global variable to be seen in the whole file.

import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './Leaderboard.css';
import './sliders.less';
import bs from './bootstrap/css/bootstrap.min.css';
import './share_icon.css';
import * as firebase from 'firebase';
import { pressButton } from './api';
import { SocketComponent } from './api';
import {Slider} from './sliders';
import {Helmet} from 'react-helmet';
import fa from './font-awesome/css/font-awesome.min.css';

//gooey nav cuases errors with the loader, it's deprecated, so can't use it, peerdependencies and things ont work.
//import GooeyNav from 'react-gooey-nav';
//console.log(GooeyNav);
// Initialize Firebase clientside
var config = {
  apiKey: "AIzaSyD6XJ6SUg_Zk2lRukm9jypmGtSjDzHDQ58",
  authDomain: "dont-press-that.firebaseapp.com",
  databaseURL: "https://dont-press-that.firebaseio.com",
  projectId: "dont-press-that",
  storageBucket: "",
  messagingSenderId: "1068081005377"
};
firebase.initializeApp(config);
    const topScoresRef = firebase.database().ref().child('topscores');

    //the state of the middle button
    //add a listener whenever this value changes
    
    topScoresRef.on("value", function(){
      //loop through and update the scores table

      //delete all of the LeaderBoardRow components that are not relevant.
      //query and add the new rows in.
    });
    //SHOULD SEND OWN CURSOR POSITION TO SERVER
//var points = 0;
//init express, socketIO
//var express = require('express');
//var app = express();
//var server = require('http').Server(app); //.createServer(app).listen(8080);
//var io = require('socket.io').listen(server);

//app.use('/css', express.static(__dirname + '/css'));
//app.use('/js', express.static(__dirname + '/js'));
//app.use('/assets', express.static(__dirname + '/assets'));

// app.get('/', function(req, res){
//   res.sendFile(__dirname + '/index.html');
// });

// server.listen(8081, function() {
//   console.log('Listening on ' + server.address().port);
// });

//var my_score = 0;
//var globalVar = 0;
export const updateScore = (new_score) => {
  console.log("drawing updated score as : ", new_score);
 // my_score = new_score;
  //this.forceUpdate();
  //setScore(new_score);
}

// function setScore(new_score){
//   this.setState({
//       score : new_score
//   });
// }
//<img src={logo} className="App-logo" alt="logo" />

class App extends Component {
  //the props here would be passed in from index.js
  //a state is similar to props, but it is private and fully controlled by the componenet
  //MVC model, model, deals with the database, View deals witht he UI and what is seen, Controller deals with the JS code, and functionality and interactivity.
  // constructor(props){
  //   super(props);
  //   //console.log("PROPS: ", props);
  //   //initListeners();

  //   //bind the function to this component, so it can access private methods.
  //   //this.setScore = setScore.bind(this);

  //   // this.state = {
  //   //   score : this.props
  //   // }
  // }

    // globalVar.callback = (t_score) => {
    //   this.setState = ({
    //     score : t_score
    //   });
    // }
  //lifecycle function.
  componentDidMount(){
    //get all positions of cursors constantly.
    //SHOUDL RECEIVE UPDATES FROM SERVER
    //const cursorsRef = firebase.database().ref().child('cursors');
    //const pressedRef = firebase.database().ref().child('button_details');
    //update and draw all of the cursor positions.
    window.fbAsyncInit = function(){
      FB.init({
        appId: '2029822293895940',
        cookie: true,
        xfbml: true,
        version: 'v2.1'
      });

      FB.AppEvents.logPageView(); 

      FB.getLoginStatus(function(response){
        this.statusChangeCallback(response);
      }.bind(this));
    }.bind(this);

    //load the Facebook SDK asynchronously
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if(d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  //run a very simple test of the graph API after login is successful
  // successful. See statusChangeCallback() for when thsi call is made
  testAPI = function(){
    console.log("fethcing facebook information");
    FB.api('/me', function(response){
      console.log('Successful loging for: ' + response.name);
      document.getElementById('^status').innerHTML = 
        'Thanks for logging in, ' + response.name + '!';
    });
  }

  //this is called with the results from the FB.getLoginStatus()
  statusChangeCallback = function(response){
    console.log('statusChangeCallback');
    console.log("response: " , response);
    if(response.status === 'connected'){
      //logged into app and facebook
      this.testAPI();
    }
    else if(response.status === 'not_authorized'){
      //The person is logged into facebook, but not the app
      document.getElementById('status').innerHTML = 'Please Log into this app.';
    }
    else{
      //the person is not logged into facebook
      //not sure if they are logged into this app or not
      document.getElementById('status').innerHTML = 'lease log into Facebook.';
    }
  }

  //handles the login click
  checkLoginState = function(){
    FB.getLoginStatus(function(response){
      this.statusChangeCallback(response);
    }.bind(this));
  }
  handleClick = function(){
    FB.login(this.checkLoginState());
  }


//<Slider/>
  render() {
    return (
      <div className="App">
        <header className="App-header"></header>
        <Settings/>
          <span className="App-title"><b>D</b>on't <b>P</b>ress <b>T</b>he <b>B</b>utton</span>
          
          <SocketComponent/>
          
          <Game/>
          <Instructions/>
      </div>
    );
  }
}
///
class Instructions extends React.Component{
  render(){
    return(
      <div className="instructions-div">
        <u><b>
        <span className = "seconds-title">Instructions: </span><br></br>
        </b></u>
        <div id="instructions-align-div">

          <span className="seconds"><b>1. </b>Press the button and hope no one else is pressing it.</span>
          <br></br>
          <span className="seconds"><b>2. </b>If someone else has pressed in the last three seconds, your points will reset</span>
          <br></br>
          <span className="seconds"><b>3. </b>Try and reach the highest score</span>
          <br></br><br></br>
          <div id="versioning-div">
            <span className="versioning">v0.0.1 last updated November 19th 2017 by juuno.co</span>
          </div>
        </div>
      </div>
    );
  }
}
///
class Score extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      score : this.props.score
    };
  }
  componentDidMount(props){
    this.setState ({
      score : this.props.score
    });
  }
  //the original value of the props (score) changes, so this method is called.
  componentWillReceiveProps(nextProps){
    //console.log("COMPONENTWILLRECEIEVEPROPS WAS CALLED: ", nextProps);
    if(this.props.score !== nextProps.score){
      this.setState({
        score : nextProps.score
      });
    }
  }
  render(){
    return(
      <div className="score-wrapper" onClick={playerPressedButton}>
        <span className="App-intro">
        { 
          this.state.score
        } 
        </span>
      </div>
    )
  }
}
///
class Game extends React.Component {
  render(){
    return (
      <div className="App">
        <div className="game-info">
          <div>{   }</div>
          <ol>{}</ol>
        </div>
      </div>
    );
  }
}

//dont press button object / component
class DontPressButton extends Component {

  constructor(props){
    super(props);
    this.state = {
      seconds_remaining : this.props.remaining_time
    }
  }

  componentWillReceiveProps(nextProps){
    //console.log("COMPONENTWILLRECEIEVEPROPS WAS CALLED: ", nextProps);
    if(this.props.remaining_time !== nextProps.remaining_time){
      this.setState({
        seconds_remaining : nextProps.remaining_time--
      });
    }
  }

  render(){
    return (
      <button className="the-button" onClick={playerPressedButton}>
          {isNaN(this.state.seconds_remaining) ? 0 : this.state.seconds_remaining }

      </button>
    );//<img src={logo} className="App-logo" alt="logo" />        //{this.props.text}
  }
}

function playerPressedButton(){
  console.log("Button was pressed!!!");
  //send to server that you pressed a button.
  //hardcoded the name for now
  pressButton();
}

class ShareDiv extends React.Component{
  componentDidMount(){
    document.getElementById('fa-circle').onclick = function(){
      FB.ui({
        method: 'share',
        display: 'popup',
        href: 'http://www.dontpressthat.com'
      }, function(response){});
    }
  }
  render(){
    return (
      <div>
        <div id="share-div">
            <span className="share-div-title">Find <b>DPTB</b> interesting?</span>
            <br></br> <br></br>
            <ShareButtons/>
                      
        </div>
      </div>
    );
  }
}

class ShareButtons extends React.Component{
  render(){
    return(
      <div>
        <ul className="list-inline">
          <li className="list-inline-item">
            <a className="btn-social btn-outline" href="#">
              <i className="fa fa-fw fa-facebook" id="fa-circle"></i>
            </a>
          </li>
          <li className="list-inline-item">
            <a className="btn-social btn-outline twitter-share-button" target="_blank"  rel="noopener noreferrer" href="https://twitter.com/intent/tweet">
              <i className="fa fa-fw fa-twitter"></i>
            </a>
          </li>
        </ul>
      </div>

    );
  }
}

//<li className="share-div-title">Share it with your friends</li>
//<span className="share-div-title">Watch it grow</span>
class EmptyDiv extends React.Component{
  render(){
    return (
      <div id="empty-div">
      <StillToCome/>
      </div>
    );
  }
}
///
class StillToCome extends React.Component{
  render(){
    return (
      <div id="still-to-come-div">
        <span className="lookforward-span">{"We're still Developing this app"}</span>
        <br></br>
        <span className="lookforward-span">{"Here's what you can look forward to: "}</span>
        <ul>
          <li className="lookforward-span">A Chat Interface to see what's happening</li>
          <li className="lookforward-span">Being able to save your state between plays</li>
          <li className="lookforward-span">Cool graphics (We know the site looks a bit dull)</li>
          <li className="lookforward-span">And More... :)</li>
        </ul>
      </div>
    );
  }
}
///
class Ads extends React.Component{
  render(){
    return (
      <div>
          <div id="ad-left">{}</div>
          <div id="ad-right">{}</div>
      </div>
    );
  }
}
///

class Settings extends React.Component{
  render(){
    return(
      <div>
        <Helmet
          meta={[
            {"property": "og:type", "content": "video.other"},
            
            {"property": "og:title", "content": "DPTB"},
            {"property": "og:url", "content": "https://www.dontpressthat.com"},
            {"property": "og:description", "content": "Please, Dont Press The Button"}
          ]}
        />
      </div>
    );
  }
}
///{"property": "og:image", "content": "https://www.w3schools.com/css/trolltunga.jpg"}

export { Score, DontPressButton, ShareDiv, EmptyDiv };
export default App;

//classes created in other files will just have the keyword 'export default' in front
//const { className, ...props } = this.props;
//<div className= { classnames('About', className)} { ...props}>

//use react-router to define the routes of things.
//http://localhost:3000 to see the source code